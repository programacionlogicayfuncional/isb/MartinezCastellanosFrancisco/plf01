(ns plf01.core)

;comparaciones <,>,==,<=,>=

(defn funcion-+-1
  [a]
  (< a))


(defn funcion-+-2
  [a b]
  (<= a b))

(defn funcion-+-3
  [a b]
  (== a b))

(funcion-+-1  5 )
(funcion-+-2  1 2)
(funcion-+-3  3 3)

; comparaciones parte 2

(defn funcion-+-4
  [a b]
  (> a b))

(defn funcion-+-5
  [a b]
  (>= a b))

(defn funcion-+-6
  [a]
  (> a))

(funcion-+-4  5 3)
(funcion-+-5  1 2)
(funcion-+-6    2)

;constains

(def funcion-contains1-
#{12 13 24}
  )

(def funcion-contains2-
  #{{}})

(def funcion-contains3-
  {"perro" "gato"})
 
(contains? funcion-contains1- 12)
(contains? funcion-contains2- {})
(contains? funcion-contains3- "perro")

;count

(defn funcion-count-1
  [cosa]
  (count cosa))

(defn funcion-count-2
[letrero]
  (counted? letrero))

(defn funcion-count-3
  [text]
  (count text))



(funcion-count-1 "XD")
(funcion-count-2 "AIAOASMMSMS")
(funcion-count-3 nil)

; disj


(defn funcion-disj-1
  [collection]
  (disj collection))

(defn funcion-disj-2
  [collection a]
  (disj collection a))

(defn funcion-disj-3
  [collection a b]
  (disj collection a b))

(funcion-disj-1 [1 "hi" false])
(funcion-disj-2 #{"el" "diablo" "anda"} "suelto")
(funcion-disj-3 #{100 123 12} 123 1 )

; false?

(defn false-1
  [a]
  (false? a))

(defn false-2
  [a]
  (false? a))

(defn false-3
  [a]
  (false? a))

(false-1 nil)
(false-2 false)
(false-3 true)

;true

(defn funcion-true-1
  [a]
  (true? a))

(defn funcion-true-2
  [a]
  (true? a))

(defn funcion-true-3
  [a]
  (true? a))

(funcion-true-1 nil)
(funcion-true-2 false)
(funcion-true-3 true)

;neg

(defn funcion-neg-1
  [a]
  (neg? a))

(defn funcion-neg-2
  [-a ]
  (neg? -a ))

(defn funcion-neg-3
  [+a]
  (neg? +a ))

(funcion-neg-1  2)
(funcion-neg-2 -1)
(funcion-neg-3  0)




